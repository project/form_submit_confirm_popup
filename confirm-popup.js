/**
 * @file
 * Javascript file for form_submit_confirm_popup.
 */

(function ($) {

	Drupal.behaviors.form_submit_confirm_popup = {
		attach: function (context, settings) {

			if (Drupal.settings.form_submit_confirm_popup !== undefined) {

				var popups = $.parseJSON(Drupal.settings.form_submit_confirm_popup.popups);
				var form_str = Drupal.settings.form_submit_confirm_popup.form_id;
				var form_id = '#' + form_str.replace('_', '-');

				$.each(popups, function (j, popup) {

					var submit_confirm_popup_wrapper = '#popup-' + j + '-submit-confirm-popup-wrapper';
					var submit_confirm_popup = '#popup-' + j + '-submit-confirm-popup';
					var submit_buttons = popup['submit_buttons'];

					$.each(submit_buttons, function (i, e) {

						var submit_button_id = '#' + e.button;

						if(e.hide === 1){
							$(submit_button_id).css('display', 'none');
						}

						$(form_id + ' ' + submit_button_id).click(function (ev) {

							if (typeof $.colorbox !== undefined && $.isFunction($.colorbox)) {
								if ($(submit_confirm_popup).parents(submit_confirm_popup_wrapper).length != 0) {
									ev.preventDefault();
									var div = $(submit_confirm_popup);
									$(div).show();
									$.colorbox({inline: true, href: div});
								}
								else {
									$(this).removeAttr('disabled');
								}
							}
							else {
								if (!confirm(e.text)) {
									ev.preventDefault();
									return false;
								}
								else {
									return true;
								}
							}
						});

						$("#confirm-yes-" + e.button).click(function () {
							$(form_id + ' ' + submit_button_id).trigger('click');
						});

						$("#confirm-no-popup-" + j).click(function () {
							$(submit_confirm_popup).colorbox.close();
						});

					})

				})

			}

		}
	};

})(jQuery);
